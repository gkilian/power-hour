import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import PowerHour from './PowerHour';

ReactDOM.render(
  <React.StrictMode>
    <PowerHour />
  </React.StrictMode>,
  document.getElementById('root')
);
