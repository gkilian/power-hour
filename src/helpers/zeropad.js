
export default function zeropad(number, amount) {
    if (number >= Math.pow(10, amount)) {
        return number;
    }

    return (Array(amount).join(0) + number).slice(-amount);
}
